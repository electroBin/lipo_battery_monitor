;Test the communication between an external device and the battery monitor
;circuit   

    list		p=16f1937	;list directive to define processor
    #include	<p16f1937.inc>		; processor specific variable definitions
	
    errorlevel -302	;no "register not in bank 0" warnings
    errorlevel -312     ;no  "page or bank selection not needed" messages
    errorlevel -207    ;no label after column one warning
	
    #define BANK0	       (h'000')
    #define BANK1	       (h'080')
    #define BANK2	       (h'100')
    #define BANK3	       (h'180')
    #define battAddrRead       (b'00000101')    ;Bus address for i2c battery (read)
					        ;Address=d'2' but bit #0 is ignored
    #define battAddrWrite      (b'00000100')    ;Bus address for i2c battery (write)
					        ;Address=d'2' but bit #0 is ignored
    #define readCell1	       (b'00000001')	;I2C command to retrieve cell1 value					     
    #define readCell2	       (b'00000010')	;I2C command to retrieve cell2 value
    #define readCell3	       (b'00000011')	;I2C command to retrieve cell3 value
    #define readCell4	       (b'00000100')	;I2C command to retrieve cell4 value					     

    __CONFIG _CONFIG1,    _MCLRE_ON & _CP_OFF & _CPD_OFF & _BOREN_OFF & _WDTE_OFF & _PWRTE_ON & _FOSC_XT & _FCMEN_OFF & _IESO_OFF

;Context saving variables:
CONTEXT	UDATA_SHR
w_temp		    RES	1	; variable used for context saving
status_temp	    RES	1	; variable used for context saving
pclath_temp	    RES	1
receiveData	    RES	1
transData	    RES	1

;General Variables
GENVAR	UDATA
userMillis	    RES	1
i2cByteToSend	    RES	1
i2cDataReceived	    RES	1
cell1		    RES	1
cell2		    RES	1
cell3		    RES	1
cell4		    RES	1
cellCounter	    RES	1	;Counter to keep track of which cell is being
				;read
				  

;**********************************************************************
    ORG		0x000	
    pagesel		start	; processor reset vector
    goto		start	; go to beginning of program
INT_VECTOR:
    ORG		0x004		; interrupt vector location
INTERRUPT:
    ;SUBROUTINES:	
;variable length mllisecond delay (number of millis needed is already in work register)
delayMillis
    banksel	userMillis
    movwf	userMillis	    ;user defined number of milliseconds
startDly
    banksel	TMR0
    clrf	TMR0
waitTmr0
    movfw	TMR0
    xorlw	.125	    ;125 * 8uS = 1mS
    btfss	STATUS, Z
    goto	waitTmr0
    banksel	userMillis
    decfsz	userMillis, f   ;reached user defined milliseconds yet?
    goto        startDly
    
    retlw	0
;***************************I2C subroutines*************************************
;Send START condition and wait for it to complete
I2Cstart
    pagesel	waitMSSP
    call	waitMSSP
    pagesel$
    banksel	SSPCON2
    bsf		SSPCON2, SEN
    btfsc	SSPCON2, SEN
    goto	$-1
    retlw	0
    
;Send STOP condition and wait for it to complete
I2CStop
    pagesel	waitMSSP
    call	waitMSSP
    pagesel$
    banksel	SSPCON2
    bsf		SSPCON2, PEN
    btfsc	SSPCON2, PEN	    ;PEN auto cleared by hardware when finished
    goto	$-1
    retlw	0
    
;Send RESTART condition and wait for it to complete
I2Crestart
    pagesel	waitMSSP
    call	waitMSSP
    pagesel$
    banksel	SSPCON2
    bsf		SSPCON2, RSEN
    retlw	0
    
    ;Send ACK to slave (master is in receive mode)
sendACK
    pagesel	waitMSSP
    call	waitMSSP
    pagesel$
    banksel	SSPCON2
    bcf		SSPCON2, ACKDT  ;(0=ACK will be sent)
    bsf		SSPCON2, ACKEN	;(ACK is now sent)
    retlw	0
    
;Send NACK to slave (master is in receive mode)
sendNACK
    pagesel	waitMSSP
    call	waitMSSP
    pagesel$
    banksel	SSPCON2
    bsf		SSPCON2, ACKDT  ;(1=NACK will be sent)
    bsf		SSPCON2, ACKEN	;(NACK is now sent)
    retlw	0

;Enable Receive Mode
enReceive
    pagesel	waitMSSP
    call	waitMSSP
    pagesel$
    banksel	SSPCON2
    bsf		SSPCON2, RCEN
    btfss	SSPCON2, RCEN
    goto	$-1
    retlw	0
    
;I2C wait routine   
waitMSSP
    banksel	SSPSTAT
    btfsc	SSPSTAT, 2	;(1=transmit in progress, 0=no trans in progress
    goto	$-1		;trans in progress so wait
    banksel	SSPCON2
    movfw	SSPCON2		;get copy of SSPCON2
    andlw	b'00011111'	;mask out bits that specify something going on
				;ACEKN, RCEN, PEN, RSEN, SEN = 1 then wait
    btfss	STATUS, Z	;0=all good, proceed
    goto	$-3		;1=not done doing something so retest and wait
    retlw	0
    
;Send a byte of (command or data) via I2C    
sendI2Cbyte
    banksel	SSPBUF
    movwf	SSPBUF		;byte is already in work register
    ;banksel	SSPSTAT
    ;btfsc	SSPSTAT, 2	;wait till buffer is full (when RW=1=transfer complete)
    ;goto	$-1		;not full, wait here
    pagesel	waitMSSP
    call	waitMSSP
    pagesel$
    retlw	0
    
;Write to slave device    
I2Csend
    ;Send data and check for error, wait for it to complete
    banksel	i2cByteToSend
    movfw	i2cByteToSend
    call	sendI2Cbyte	    ;load data into buffer
    banksel	SSPCON2
    btfsc	SSPCON2, ACKSTAT    ;ACKSTAT=1 if ACK not received from slave
    goto	$-1	            ;ACK not received
    retlw	0  

waitACK
    banksel	SSPCON2
    btfsc	SSPCON2, ACKSTAT
    goto	$-1
    retlw	0
    
;*************UART TEST ROUTINE
    Transmit
    banksel	transData
    movfw	transData	
    banksel	TXREG
    movwf	TXREG		;data to be transmitted loaded into TXREG
				;and then automatically loaded into TSR
    nop
    nop
wait_trans
    banksel	PIR1
    btfss	PIR1, TXIF	;Is TX buffer full? (1=empty, 0=full)
    goto	wait_trans
    retlw	0
    
;******************Perform a reading of battery cell voltages*******************
GetBattery
;Start communication    
    pagesel	I2Cstart
    call	I2Cstart
    pagesel$
    movlw	battAddrWrite
    banksel	i2cByteToSend
    movwf	i2cByteToSend
    pagesel	I2Csend
    call	I2Csend
    pagesel$
    ;Wait for ACK from slave
    pagesel	waitACK
    call	waitACK
    pagesel$

;Determine which cell we need to read
    movlw	.0		;Cell #1?
    banksel	cellCounter
    xorwf	cellCounter, w
    btfsc	STATUS, Z
    goto	Cell1Command
    
    movlw	.1		;Cell #2?
    banksel	cellCounter
    xorwf	cellCounter, w
    btfsc	STATUS, Z
    goto	Cell2Command
    
    movlw	.2		;Cell #3?
    banksel	cellCounter
    xorwf	cellCounter, w
    btfsc	STATUS, Z
    goto	Cell3Command
    
    movlw	.3		;Cell #4?
    banksel	cellCounter
    xorwf	cellCounter, w
    btfsc	STATUS, Z
    goto	Cell4Command
    
;Load the appropriate command
Cell1Command
    movlw	readCell1
    goto	SendIt
Cell2Command
    movlw	readCell2
    goto	SendIt  
Cell3Command
    movlw	readCell3
    goto	SendIt
Cell4Command
    movlw	readCell4
    goto	SendIt    

;Send the command
SendIt
    banksel	i2cByteToSend
    movwf	i2cByteToSend
    pagesel	I2Csend
    call	I2Csend
    pagesel$
    ;Wait for ACK from slave
    pagesel	waitACK
    call	waitACK
    pagesel$
;Send Stop
    pagesel     I2CStop
    call	I2CStop
    pagesel$
;Read cell data
    pagesel	I2Cstart
    call	I2Cstart
    pagesel$
    movlw	battAddrRead
    banksel	i2cByteToSend
    movwf	i2cByteToSend
    pagesel	I2Csend
    call	I2Csend
    pagesel$
;Wait for ACK from slave
    pagesel	waitACK
    call	waitACK
    pagesel$
    
;Receive byte:
    pagesel	enReceive
    call	enReceive
    pagesel$
    pagesel	waitMSSP
    call	waitMSSP
    pagesel$
    
;Determine which cell we are receiving
    movlw	.0		;Cell #1?
    banksel	cellCounter
    xorwf	cellCounter, w
    btfsc	STATUS, Z
    goto	StoreCell1
    
    movlw	.1		;Cell #2?
    banksel	cellCounter
    xorwf	cellCounter, w
    btfsc	STATUS, Z
    goto	StoreCell2
    
    movlw	.2		;Cell #3?
    banksel	cellCounter
    xorwf	cellCounter, w
    btfsc	STATUS, Z
    goto	StoreCell3
    
    movlw	.3		;Cell #4?
    banksel	cellCounter
    xorwf	cellCounter, w
    btfsc	STATUS, Z
    goto	StoreCell4
    
    ;Store the cell reading
StoreCell1    
    banksel	SSPBUF
    movfw	SSPBUF		;Read from buffer and store data.
    banksel	cell1
    movwf	cell1
    banksel	cellCounter
    incf	cellCounter,f	;Increment the counter.
    goto	DoneStoring
StoreCell2    
    banksel	SSPBUF
    movfw	SSPBUF		;Read from buffer and store data.
    banksel	cell2
    movwf	cell2
    banksel	cellCounter
    incf	cellCounter,f	;Increment the counter.
    goto	DoneStoring   
StoreCell3    
    banksel	SSPBUF
    movfw	SSPBUF		;Read from buffer and store data.
    banksel	cell3
    movwf	cell3
    banksel	cellCounter
    incf	cellCounter,f	;Increment the counter.
    goto	DoneStoring    
StoreCell4    
    banksel	SSPBUF
    movfw	SSPBUF		;Read from buffer and store data.
    banksel	cell4
    movwf	cell4
    banksel	cellCounter
    clrf	cellCounter	;Reset counter
    goto	DoneStoring    
    
DoneStoring    
    ;Send ACK
    pagesel	sendNACK
    call	sendNACK
    pagesel$
    ;Send Stop
    pagesel     I2CStop
    call	I2CStop
    pagesel$
    
    retlw	0
start:

    banksel BANK1
    ;Set PORTS to output
    movlw	b'00000000'		     
    movwf	(TRISA ^ BANK1)
    movlw	b'00000010'
		;------0-	    ;PORTB, 1 for signal for battery read
    movwf	(TRISB ^ BANK1)
    movlw	b'11011000'
		 ;---11---	    :I2C, SDA and SCL as inputs
    movwf	(TRISC ^ BANK1)
    movlw	b'00000000'
    movwf	(TRISD ^ BANK1)
    movlw	b'00000000'
    movwf	(TRISE ^ BANK1)
    
    banksel	ANSELA
    movlw	b'00000000'
    movwf	ANSELA
    clrf	ANSELB                    
    clrf	ANSELD
    clrf	ANSELE
    
    banksel	PORTB
    clrf	PORTB
  
    movlw	b'01101000'
    banksel	OSCCON
    movwf	OSCCON
   
;************************Configure timer************************************
    ;With 4Mhz external crystal, FOSC is not divided by 4.
    ;Therefore each instruction is 1/4 of a microsecond (250*10^-9 sec.)
    movlw	b'11000100'	
		 ;1-------	WPUEN=0, all weak pull-ups are disabled
		 ;-1------	INTEDG=1, Interrupt on rising edge of INT pin
		 ;--0-----	TMR0CS=0, TMR0 clock source=internal instruction
			        ;	  (FOSC/4)
		 ;---0----	;TMR0SE=0, disregard
		 ;----0---	;PSA=0, prescaler assigned to TMR0 module
		 ;-----100	;PS<2:0> = 00, TMRO increments once every 32
				;instruction cycles 
				;Every instruction cycle is 250*10^-9 sec (4Mhz), 
				;therefore TMR0 increments once every 32 * 250*10^-9 sec
				;or once every 8uS
    banksel	OPTION_REG	
    movwf	OPTION_REG
    
    ;external crystal:
    movlw	b'00000000'
    banksel	OSCCON
    movwf	OSCCON
    ;*********************************CONFIGURE UART********************************
    ;Configure Baud rate
    movlw	b'00111010' ;=58
    banksel	SPBRG
    movwf	SPBRG	    
    
    movlw	b'00000011' ;=3 Total value of SPBRG/n = 826
    banksel	SPBRGH
    movwf	SPBRGH
    
    banksel	BAUDCON
    movlw	b'00001000'
		 ;----1---	BRG16 (16 bit baud rate generator)
    movwf	BAUDCON
    
    ;Enable Transmission:
    movlw	b'00100000'
		 ;-0------  :8-bit transmission (TX9 = 0)
		 ;--1-----  :Enable transmission (TXEN = 1)
		 ;---0----  :Asynchronous mode (SYNC = 0)
		 ;-----0--  :Low speed baud rate (BRGH = 0)
    banksel	TXSTA
    movwf	TXSTA
		 
    ;Enable Reception:
    movlw	b'10010000'
		 ;1-------  :Serial port enabled (SPEN = 1)
		 ;-0------  :8-bit reception (RX9 = 0)
		 ;---1----  :Enable receiver (CREN = 1)
		 ;----0---  :Disable address detection (ADDEN = 0)
    ;			     all bytes are received and 9th bit can be used as
    ;			     parity bit
    banksel	RCSTA
    movwf	RCSTA
    
;*******************************************************************************
    ;*********************Configure I2C*****************************************
    movlw	.255	;SCL pin clock period=FOSC/(4*(SSPADD+1))
				;SSPADD=255.
				;Baud=(4*10^6) / (4*(103+1)) = 3900 bps
    banksel	SSPADD
    movwf	SSPADD
    
    movlw	b'10000000'
		 ;1-------	'SMP=1, data sampled at end of data output time
				;slew rate control in 100kHz mode
    banksel	SSPSTAT
    movwf	SSPSTAT
    
    movlw	b'00101000'
		 ;-0------	;SSPOV=receive overflow indicator (read data in 
				;sspbuf before new data comes in to prevent error.
				;Check and clear to ensure sspbuf can be updated
		 ;--1-----	;SSPEN=1 (Enable I2C serial port)
		 ;----1000	;1000 = I2C Master Mode. 
				;Clock=FOSC/(4*(SSPADD+1))
    banksel	SSPCON1
    movwf	SSPCON1
    banksel	SSPCON2
    clrf	SSPCON2
    
    movlw	.10
    pagesel	delayMillis
    call	delayMillis
    pagesel$
    
    banksel	userMillis
    clrf	userMillis	
    clrf	receiveData
    clrf	transData
    
    banksel	PORTD
    clrf	PORTD
    clrf	PORTB
    movlw	.255
    call	delayMillis
    banksel	cell1
    clrf	cell1
    clrf	cell2
    clrf	cell3
    clrf	cell4
    clrf	cellCounter
    
;***************************MAIN************************************************
mainLoop
    banksel	PORTB	    ;PORTB, 1 and 2 are LEDs/inputs used for triggering 
			    ;and testing purposes
    btfss	PORTB, 1
    goto	mainLoop
    movlw	.25
    call	delayMillis
    banksel	PORTB
    btfss	PORTB, 1
    goto	mainLoop
    ;Test i2c communication with battery monitor
    banksel	PORTB
    bsf		PORTB, 2

    pagesel	GetBattery	;Get cell1 value
    call	GetBattery
    pagesel$
    
    pagesel	GetBattery	;Get cell2 value
    call	GetBattery
    pagesel$
    
    pagesel	GetBattery	;Get cell3 value
    call	GetBattery
    pagesel$
    
    pagesel	GetBattery	;Get cell4 value
    call	GetBattery
    pagesel$
    
    
    ;*******UART TEST TO CONFIRM WE RECEIVED CORRECT CELL VALUES****************
    banksel	cell1
    movfw	cell1
    movwf	transData
    pagesel	Transmit
    call	Transmit
    pagesel$
    
    banksel	cell2
    movfw	cell2
    movwf	transData
    pagesel	Transmit
    call	Transmit
    pagesel$
    
    banksel	cell3
    movfw	cell3
    movwf	transData
    pagesel	Transmit
    call	Transmit
    pagesel$
    
    banksel	cell4
    movfw	cell4
    movwf	transData
    pagesel	Transmit
    call	Transmit
    pagesel$
    ;***************************END UART TEST***********************************
    
stillPressed
    banksel	PORTB
    btfsc	PORTB, 1
    goto	stillPressed
    bcf		PORTB, 2
    goto	mainLoop
    
DoneTesting
    ;goto	DoneTesting
    
    END                       





















