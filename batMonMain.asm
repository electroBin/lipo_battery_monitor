;Monitor a 4 cell LiPo battery

    list	p=16f1937	;list directive to define processor
    #include	<p16f1937.inc>	; processor specific variable definitions
	
    errorlevel -302	;no "register not in bank 0" warnings
    errorlevel -312     ;no  "page or bank selection not needed" messages
    errorlevel -207    ;no label after column one warning
	
    #define BANK0	       (h'000')
    #define BANK1	       (h'080')
    #define BANK2	       (h'100')
    #define BANK3	       (h'180')
    #define battAddrRead       (b'00000101')    ;Bus address for i2c battery (read)
					        ;Address=d'2' but bit #0 is ignored
    #define battAddrWrite      (b'00000100')    ;Bus address for i2c battery (write)
					        ;Address=d'2' but bit #0 is ignored
    #define readCell1	       (b'00000001')	;I2C command to retrieve cell1 value					     
    #define readCell2	       (b'00000010')	;I2C command to retrieve cell2 value
    #define readCell3	       (b'00000011')	;I2C command to retrieve cell3 value
    #define readCell4	       (b'00000100')	;I2C command to retrieve cell4 value

    __CONFIG _CONFIG1,    _MCLRE_ON & _CP_OFF & _CPD_OFF & _BOREN_OFF & _WDTE_OFF & _PWRTE_ON & _FOSC_HS & _FCMEN_OFF & _IESO_OFF

;Context saving variables:
MULTIBANK	    UDATA_SHR
w_copy		    RES 1	;variable used for context saving (work reg)
status_copy	    RES 1	;variable used for context saving (status reg)
pclath_copy	    RES 1	;variable used for context saving (pclath copy
userMillis	    RES	1
dly16Ctr	    RES	1
		    
;General Variables
GENVAR		    UDATA
;Variables to hold individual values for the four battery cells
cell1		    RES	1
cell2		    RES	1
cell3		    RES	1
cell4		    RES	1
command		    RES	1	;Command sent from master
i2cByteToSend	    RES	1	;Data to be sent from slave		
receiveData	    RES	1
transData	    RES	1

;**********************************************************************
    ORG		0x000	
    pagesel		start	; processor reset vector
    goto		start	; go to beginning of program
INT_VECTOR:
    ORG		0x004		; interrupt vector location
INTERRUPT:
    banksel	w_copy
    movwf       w_copy           ;save off current W register contents
    movfw        STATUS         ;move status register into W register
    movwf       status_copy      ;save off contents of STATUS register
    movf        PCLATH,W
    movwf       pclath_copy
    
    ;Determine if STOP bit triggered interrupt
    banksel	SSPSTAT
    btfsc	SSPSTAT, P
    goto	BatteryReadingDone
    
    ;For I2C interrupt, clear PIR1, SSPIF flag. A bus collision also causes
    ;an interrupt so clear PIR2, BCLIF also. After interrupt is triggered upon 
    ;address match, SSPSTAT, BF (Buffer full status) must be cleared by reading
    ;SSPBUF
  
    ;Check for collisions and overflows
    banksel	PIR2
    bcf		PIR2, BCLIF
    banksel	SSPCON1
    btfsc	SSPCON1, SSPOV
    bcf		SSPCON1, SSPOV
    btfsc	SSPCON1, WCOL
    bcf		SSPCON1, WCOL
    
    clrf	INTCON
    banksel	PIE1
    bcf		PIE1, SSPIE	;Disable serial port interrupts while we do this
	;ACK pulse sent by slave on the 9th bit. (hardware)
	;After ACK, slave hardware clears SSPCON1, CKP and SCL pin is held low 
	;(Master can't send anything during this time while slave prepares data to be transmitted)
    ;Read the received address from SSPBUF to clear SSPSTAT, BF flag
    banksel	SSPBUF
    movfw	SSPBUF
    ;Test R/W bit to determine if this is a read or a write:
    banksel	SSPSTAT
    btfsc	SSPSTAT, 2	;R/W=1 read, R/W=0 write (receive command)
    goto	SlaveRead
    
SlaveWrite;**********SLAVE IS RECEIVING A COMMAND*******************************
    banksel	PIR1
    bcf		PIR1, SSPIF	;Clear interrupt flag
    ;Get command from master
    banksel	SSPBUF
    movfw	SSPBUF
    banksel	command
    movwf	command
    
    ;Determine the value for the command and load appropriate cell value
    movlw	readCell1	;Cell #1?
    xorwf	command, w  
    btfsc	STATUS, Z	
    goto	loadCell1
    
    movlw	readCell2	;Cell #2?
    xorwf	command, w  
    btfsc	STATUS, Z	
    goto	loadCell2
    
    movlw	readCell3	;Cell #3?
    xorwf	command, w  
    btfsc	STATUS, Z	
    goto	loadCell3
    
    movlw	readCell4	;Cell #4?
    xorwf	command, w  
    btfsc	STATUS, Z	
    goto	loadCell4
    
    goto	BatteryReadingDone
    
    ;Load the appropriate cell value. This value will be sent upon next slave-read
    ;command.
loadCell1
    banksel	cell1
    movfw	cell1
    movwf	i2cByteToSend	    ;Cell1 value is ready to be sent when master
				    ;sends the next slave-read command.
    banksel	SSPCON1
    bsf		SSPCON1, CKP	    ;Release clock line
    goto	BatteryReadingDone  ;Exit interrupt and wait for read command
    
loadCell2
    banksel	cell2
    movfw	cell2
    movwf	i2cByteToSend	    ;Cell2 value is ready to be sent when master
				    ;sends the next slave-read command.
    banksel	SSPCON1
    bsf		SSPCON1, CKP	    ;Release clock line
    goto	BatteryReadingDone  ;Exit interrupt and wait for read command    
    
loadCell3
    banksel	cell3
    movfw	cell3
    movwf	i2cByteToSend	    ;Cell3 value is ready to be sent when master
				    ;sends the next slave-read command.
    banksel	SSPCON1
    bsf		SSPCON1, CKP	    ;Release clock line
    goto	BatteryReadingDone  ;Exit interrupt and wait for read command   
    
loadCell4
    banksel	cell4
    movfw	cell4
    movwf	i2cByteToSend	    ;Cell4 value is ready to be sent when master
				    ;sends the next slave-read command.
    banksel	SSPCON1
    bsf		SSPCON1, CKP	    ;Release clock line
    ;re-sample cells:
    pagesel	SampleCells
    call	SampleCells
    pagesel$
    
    goto	BatteryReadingDone  ;Exit interrupt and wait for read command    
    
SlaveRead;***************SLAVE IS SENDING CELL VOLTAGE DATA*********************
    banksel	PIR1
    bcf		PIR1, SSPIF	;Clear interrupt flag
    pagesel	sendI2Cbyte
    call	sendI2Cbyte
    pagesel$
    banksel	SSPCON1
    bsf		SSPCON1, CKP	;Release clock line and send byte
    ;call	waitACK		;Wait for ACK from master

    bcf		INTCON, IOCIF	;Clear Interrupt on Change flag bit of INTCON
    goto	BatteryReadingDone
   
BatteryReadingDone
    banksel	pclath_copy
    movfw	pclath_copy
    movwf	PCLATH
    movf	status_copy,w   ;retrieve copy of STATUS register
    movwf	STATUS          ;restore pre-isr STATUS register contents
    swapf	w_copy,f
    swapf	w_copy,w
    ;Clear any lingering flags
    banksel	PIR1
    bcf		PIR1, SSPIF
    banksel	PIR2
    bcf		PIR2, BCLIF
    banksel	PIE1
    bsf		PIE1, SSPIE	;Re-enable serial port interrupts
    movlw	b'11000000'
    movwf	INTCON
    retfie
    
;***************************I2C subroutines*************************************
sendI2Cbyte
    banksel	i2cByteToSend
    movfw	i2cByteToSend
    banksel	SSPBUF
    movwf	SSPBUF		;byte is already in work register
    ;banksel	SSPSTAT
    ;btfsc	SSPSTAT, BF	;wait till buffer is empty (when BF=0, transmit complete)
    ;goto	$-1		;not full, wait here
   
    retlw	0
waitNack
    banksel	SSPCON2
    btfss	SSPCON2, ACKSTAT
    goto	$-1
    retlw	0
waitACK
    banksel	SSPCON2
    btfsc	SSPCON2, ACKSTAT
    goto	$-1
    retlw	0
;**************************END i2c ROUTINES*************************************   
    
;**************Perform and ADC reading for each battery cell********************
SampleCells
    banksel	cell1
    clrf	cell1
    clrf	cell2
    clrf	cell3
    clrf	cell4
;Start Sampling Cells
;****************Sample Cell #1 (AN0)***************************************
;Set AN0 as analog input for AD conversion and start AD conversion
    movlw	b'00000001'
		; -00000--  CHS<0:4> (bits 2-6) = 00000 = pin AN0/PORTA, 0 as analog input
		; ------0-  stop AD conversion
		; -------1  Enable ADC
    banksel	ADCON0
    movwf	ADCON0
    pagesel	Delay16Us
    call	Delay16Us
    pagesel$
    pagesel	Delay16Us
    call	Delay16Us
    pagesel$
    banksel	ADCON0
    bsf		ADCON0, GO
waitcell1
    btfsc	ADCON0, NOT_DONE
    goto	waitcell1
   
    pagesel	ProcessCells
    call	ProcessCells	;ProcessCells places correct jump for lookup table
    pagesel$			;into the work register
    pagesel	cellLookup
    call	cellLookup	;Encoded vale for cell voltage is in work register
    pagesel$			;4 MSBs are for left of decimal place, 4 LSBs are for
				;right of decimal place
    ;Test for ADC value for cell 1
    ;movlw	.1
    ;subwf	ADRESH, f
    ;banksel	ADRESH
    ;movfw	ADRESH
    ;banksel	cell1
    ;movwf	cell1
    
    banksel	cell1
    movwf	cell1
;******************Sample Cell #2 (AN1)***************************************** 
;Set AN1 as analog input for AD conversion and start AD conversion
    movlw	b'00000101'
		; -00001--  CHS<0:4> (bits 2-6) = 00001 = pin AN1/PORTA, 1 as analog input
		; ------0-  stop AD conversion
		; -------1  Enable ADC
    banksel	ADCON0
    movwf	ADCON0
    pagesel	Delay16Us
    call	Delay16Us
    pagesel$
    pagesel	Delay16Us
    call	Delay16Us
    pagesel$
    banksel	ADCON0
    bsf		ADCON0, GO
waitcell2
    btfsc	ADCON0, NOT_DONE
    goto	waitcell2
    
    pagesel	ProcessCells
    call	ProcessCells	;ProcessCells places correct jump for lookup table
    pagesel$			;into the work register
    pagesel	cellLookup
    call	cellLookup	;Encoded vale for cell voltage is in work register
    pagesel$			;4 MSBs are for left of decimal place, 4 LSBs are for
				;right of decimal place
    banksel	cell2
    movwf	cell2
    ;******************Sample Cell #3 (AN2)***************************************** 
    ;Set AN2 as analog input for AD conversion and start AD conversion
    movlw	b'00001001'
		; -00010--  CHS<0:4> (bits 2-6) = 00010 = pin AN2/PORTA, 2 as analog input
		; ------0-  stop AD conversion
		; -------1  Enable ADC
    banksel	ADCON0
    movwf	ADCON0
    pagesel	Delay16Us
    call	Delay16Us
    pagesel$
    pagesel	Delay16Us
    call	Delay16Us
    pagesel$
    banksel	ADCON0
    bsf		ADCON0, GO
waitcell3
    btfsc	ADCON0, NOT_DONE
    goto	waitcell3
    
    pagesel	ProcessCells
    call	ProcessCells	;ProcessCells places correct jump for lookup table
    pagesel$			;into the work register
    pagesel	cellLookup
    call	cellLookup	;Encoded vale for cell voltage is in work register
    pagesel$			;4 MSBs are for left of decimal place, 4 LSBs are for
				;right of decimal place
    banksel	cell3				
    movwf	cell3
;******************Sample Cell #4 (AN3)***************************************** 
    ;Set AN4 as analog input for AD conversion and start AD conversion
    movlw	b'00010001'
		; -00100--  CHS<0:4> (bits 2-6) = 00100 = pin AN4/PORTA, 5 as analog input
		; ------0-  stop AD conversion
		; -------1  Enable ADC
    banksel	ADCON0
    movwf	ADCON0
    pagesel	Delay16Us
    call	Delay16Us
    pagesel$
    pagesel	Delay16Us
    call	Delay16Us
    pagesel$
    banksel	ADCON0
    bsf		ADCON0, GO
waitcell4
    btfsc	ADCON0, NOT_DONE
    goto	waitcell4
   
    pagesel	ProcessCells
    call	ProcessCells	;ProcessCells places correct jump for lookup table
    pagesel$			;into the work register
    pagesel	cellLookup
    call	cellLookup	;Encoded vale for cell voltage is in work register
    pagesel$			;4 MSBs are for left of decimal place, 4 LSBs are for
				;right of decimal place
    banksel	cell4
    movwf	cell4
    retlw	0
    
;*****************End ADC reading for each battery cell*************************
    
;*******************Extract Voltage Reading from ADC value**********************
ProcessCells
    ;Does ADRESH=255 (or theoretically higher?). Cell voltage=4.2v
    movlw	.255
    banksel	ADRESH
    xorwf	ADRESH, w
    btfsc	STATUS, Z
    retlw	.0
    ;Is ADRESH between 249-254? Cell voltage=4.1v. Check if ADRESH>248.
    movlw	.249
    subwf	ADRESH, w
    btfsc	STATUS, C	;C = 0 if neg result
    retlw	.1		;This value is used to jump through lookup table
    ;Is ADRESH between 243-248? Cell voltage=4.0v. Check if ADRESH>242.
    movlw	.243
    subwf	ADRESH, w
    btfsc	STATUS, C	;C = 0 if neg result
    retlw	.2		;This value is used to jump through lookup table
    ;Is ADRESH between 237-242? Cell voltage=3.9v. Check if ADRESH>236.
    movlw	.237
    subwf	ADRESH, w
    btfsc	STATUS, C	;C = 0 if neg result
    retlw	.3		;This value is used to jump through lookup table
    ;Is ADRESH between 231-236? Cell voltage=3.8v. Check if ADRESH>230.
    movlw	.231
    subwf	ADRESH, w
    btfsc	STATUS, C	;C = 0 if neg result
    retlw	.4		;This value is used to jump through lookup table
    ;Is ADRESH between 225-230? Cell voltage=3.7v. Check if ADRESH>224.
    movlw	.225
    subwf	ADRESH, w
    btfsc	STATUS, C	;C = 0 if neg result
    retlw	.5		;This value is used to jump through lookup table
    ;Is ADRESH between 219-224? Cell voltage=3.6v. Check if ADRESH>220.
    movlw	.219
    subwf	ADRESH, w
    btfsc	STATUS, C	;C = 0 if neg result
    retlw	.6		;This value is used to jump through lookup table
    ;Is ADRESH between 213-218? Cell voltage=3.5v. Check if ADRESH>213.
    movlw	.213
    subwf	ADRESH, w
    btfsc	STATUS, C	;C = 0 if neg result
    retlw	.7		;This value is used to jump through lookup table
    ;Is ADRESH between 206-212? Cell voltage=3.4v. Check if ADRESH>206.
    movlw	.206
    subwf	ADRESH, w
    btfsc	STATUS, C	;C = 0 if neg result
    retlw	.8		;This value is used to jump through lookup table
    ;Is ADRESH between 200-205? Cell voltage=3.3v. Check if ADRESH>200.
    movlw	.200
    subwf	ADRESH, w
    btfsc	STATUS, C	;C = 0 if neg result
    retlw	.9		;This value is used to jump through lookup table
    ;Is ADRESH between 194-199? Cell voltage=3.2v. Check if ADRESH>194.
    movlw	.194
    subwf	ADRESH, w
    btfsc	STATUS, C	;C = 0 if neg result
    retlw	.10		;This value is used to jump through lookup table
    ;Is ADRESH between 188-193? Cell voltage=3.1v. Check if ADRESH>188.
    movlw	.188
    subwf	ADRESH, w
    btfsc	STATUS, C	;C = 0 if neg result
    retlw	.11		;This value is used to jump through lookup table
    ;Is ADRESH between 182-187? Cell voltage=3.0v. Check if ADRESH>182.
    movlw	.182
    subwf	ADRESH, w
    btfsc	STATUS, C	;C = 0 if neg result
    retlw	.12		;This value is used to jump through lookup table
    ;Else return 0
    retlw	.13
    
;*******************Lookup Table to get voltage values**************************
    ;4MSBs hold value for left of decimal place. 4 LSBs hold value for right of
    ;decimal place.
cellLookup
    addwf	PCL, f
    retlw	b'01000010'	;4.2v = d'66'
    retlw	b'01000001'	;4.1v = d'65'
    retlw	b'01000000'	;4.0v = d'64'
    retlw	b'00111001'	;3.9v = d'57'
    retlw	b'00111000'	;3.8v = d'56'
    retlw	b'00110111'	;3.7v = d'55'
    retlw	b'00110110'	;3.6v = d'54'
    retlw	b'00110101'	;3.5v = d'53'
    retlw	b'00110100'	;3.4v = d'52'
    retlw	b'00110011'	;3.3v = d'51'
    retlw	b'00110010'	;3.2v = d'50'
    retlw	b'00110001'	;3.1v = d'49'
    retlw	b'00110000'	;3.0v = d'48'
    retlw	b'00000000'	;0v (Low voltage warning)
    
;******************Variable millisecond delay routuine**************************
delayMillis
    movwf	userMillis	;user defined number of milliseconds
startDly
    banksel	TMR0
    clrf	TMR0
waitTmr0
    movfw	TMR0
    xorlw	.125		;125 * 8uS = 1mS
    btfss	STATUS, Z
    goto	waitTmr0
    decfsz	userMillis, f	;reached user defined milliseconds yet?
    goto	startDly
    
    retlw	0
;*******************************************************************************
    
;*****************16 microsecond delay routine**********************************
Delay16Us
    clrf	dly16Ctr	;zero out delay counter
begin
    nop				;1 uS (4Mhz clock/4 = 1uS per instruction
    banksel	dly16Ctr
    incf	dly16Ctr, f
    movlw	.16		
    xorwf	dly16Ctr, w	;16 uS passed?
    btfss	STATUS, Z
    goto	begin		;no so keep looping
    retlw	0
;*******************************************************************************
    
Transmit
    banksel	transData
    movfw	transData	
    banksel	TXREG
    movwf	TXREG		;data to be transmitted loaded into TXREG
				;and then automatically loaded into TSR
    nop
    nop
wait_trans
    banksel	PIR1
    btfss	PIR1, TXIF	;Is TX buffer full? (1=empty, 0=full)
    goto	wait_trans
    retlw	0
    
start:
    
    banksel	BANK1
    ;Set PORTS to output
    movlw	b'11111111'		
    movwf	(TRISA ^ BANK1)
    movlw	b'11110111'		    
    movwf	(TRISB ^ BANK1)
    movlw	b'11111111'	
		 ;1-------	UART RX
		 ;-1------	UART TX
		 ;----1---      I2C SCL pin
		 ;---1----	I2C SDA pin
    movwf	(TRISC ^ BANK1)
    movlw	b'00000000'
    movwf	(TRISD ^ BANK1)	    
    movlw	b'00000000'
    movwf	(TRISE ^ BANK1)
    ;************************Configure timer0************************************
    
    movlw	b'11000100'	
		 ;1-------	WPUEN=0, all weak pull-ups are disabled
		 ;-1------	INTEDG=1, Interrupt on rising edge of INT pin
		 ;--0-----	TMR0CS=0, TMR0 clock source=internal instruction
			        ;	  (FOSC/4)
		 ;---0----	;TMR0SE=0, TMR0 increments on low to high 
				;transotion of RA4/T0CKI pin
		 ;----0---	;PSA=0, prescaler assigned to TMR0 module
		 ;-----100	;PS<2:0> = 00, TMRO increments once every 32
				;instruction cycles 
				;Every instruction cycle is 250*10^-9 sec (4Mhz), 
				;therefore TMR0 increments once every 32 * 250*10^-9 sec
				;or once every 8uS
    banksel	OPTION_REG	
    movwf	OPTION_REG
    ;*********************************CONFIGURE UART********************************
    ;Configure Baud rate
    movlw	b'00111010' ;=58
    banksel	SPBRG
    movwf	SPBRG	    
    
    movlw	b'00000011' ;=3 Total value of SPBRG/n = 826
    banksel	SPBRGH
    movwf	SPBRGH
    
    banksel	BAUDCON
    movlw	b'00001000'
		 ;----1---	BRG16 (16 bit baud rate generator)
    movwf	BAUDCON
    
    ;Enable Transmission:
    movlw	b'00100000'
		 ;-0------  :8-bit transmission (TX9 = 0)
		 ;--1-----  :Enable transmission (TXEN = 1)
		 ;---0----  :Asynchronous mode (SYNC = 0)
		 ;-----0--  :Low speed baud rate (BRGH = 0)
    banksel	TXSTA
    movwf	TXSTA
		 
    ;Enable Reception:
    movlw	b'10010000'
		 ;1-------  :Serial port enabled (SPEN = 1)
		 ;-0------  :8-bit reception (RX9 = 0)
		 ;---1----  :Enable receiver (CREN = 1)
		 ;----0---  :Disable address detection (ADDEN = 0)
    ;			     all bytes are received and 9th bit can be used as
    ;			     parity bit
    banksel	RCSTA
    movwf	RCSTA
    
;*******************************************************************************
    ;*********************Configure I2C*****************************************
    movlw	b'00000100'
		 ;-------0	;Bit 0 not used in 7-bit slave mode
		 ;0000010-	;7-bit address for i2c slave mode.  Address for
				;battery monitor will be d'2'. The 1st bye received
				;after a Start or Restart is compared to the value 
				;in this register. An interrupt is generated if match
				;occurs
    banksel	SSPADD
    movwf	SSPADD
    
    movlw	b'11111110'
		 ;-------0	;Bit 0 is not used in 7-bit slave mode
		 ;11111110-	;Mask for 7-bit i2c address sent from master. Used
				;to detect address match.
    banksel	SSPMSK
    movwf	SSPMSK
    
    movlw	b'10000000'
		 ;1-------	'SMP=1, data sampled at end of data output time
				;slew rate control in 100kHz mode
    banksel	SSPSTAT
    movwf	SSPSTAT
    
    movlw	b'00111110'
		 ;-0------	;SSPOV=receive overflow indicator (read data in 
				;sspbuf before new data comes in to prevent error.
				;Check and clear to ensure sspbuf can be updated
		 ;--1-----	;SSPEN=1 (Enable I2C serial port)
		 ;---1----	;Enable Clock (CKP=1)
		 ;----1110	;I2C slave mode, 7-bit address with Start and
				;Stop bit interrupts enabled
    banksel	SSPCON1
    movwf	SSPCON1
    
    movlw	.10
    pagesel	delayMillis
    call	delayMillis
    pagesel$

    ;***********************Disable Comparators*********************************
    banksel	CM1CON0
    bcf		CM1CON0, 7
    banksel	CM2CON0
    bcf		CM2CON0, 7
    ;************************Config ADC:****************************************
    ;Configure fixed internal voltage reference:
    ;movlw	b'10000011'
		 ;1-------  Fixed Voltage reference is enabled
		 ;------11  ADC fixed reference is 4x (4.096v)
    ;banksel	FVRCON
    ;movwf	FVRCON
	    
    movlw	b'00010111' ;AN0, AN1, AN2, AN4 are inputs for 4 battery cells
    banksel	ANSELA	    ;AN3 is external voltage reference (Vref+ = 4.2V)
    movwf	ANSELA
    
    movlw	b'00010010'
		 ;0-------  ADFM=0 (left justified. 8MSBs are in ADRESH
		 ;-001----  ADCS<0:2>, bits 4-6 =001. (2.0uS)
			   ;FOSC/8=4Mhz/8 (doubles instruction cycle time)
			   ;Instruction Cycle period (TCY) now equals
			   ;2uS (greater than 1.6uS necessary for ADC)
	         ;------10 Vref+ is connected to external Vref+ (4.2 V from volage divider)
    banksel	ADCON1
    movwf	ADCON1
 
    
    banksel	ANSELB
    clrf	ANSELB
    clrf	ANSELD
    clrf	ANSELE
    
    ;call	SampleCells	;Perform initial ADC reading of each battery 
				;cell
				
;******************Enable Interrupts********************************************
    movlw	b'11000000'
	         ;1-------	;Enable global interrupts (GIE=1)
		 ;-1------	;Enable peripheral interrupts (PEIE=1)
		 ;--0-----	;Disable TMR0 interrupts (TMROIE=0)
		 ;---0----	;Disable RBO/INT external interrupt (INTE=0)
		 ;----0---	;Disable interrupt on change for PORTB (IOCIE=1)
    movwf	INTCON
    
    banksel	PIR1
    bcf		PIR1, SSPIF	;Clear MSSP interrupt flag
    
    movlw	b'00001000'
		 ;----1---	;Synchronous Serial Port (SSP) interrupts enabled.
				;Used for i2c interrupts
    banksel	PIE1
    movwf	PIE1
    
    banksel	PORTB
    clrf	PORTB
    clrf	PORTC
    clrf	PORTD
    
    banksel	cell1
    clrf	cell1
    clrf	cell2
    clrf	cell3
    clrf	cell4
    clrf	command
    banksel	transData
    clrf	transData
    clrf	receiveData
    
    clrf	userMillis
    
    ;Perform initial cell reading
    pagesel	SampleCells
    call	SampleCells
    pagesel$
    
    ;Enable interrupt on change for PORTB (currently disabled)
    movlw	b'00000000'	;PORTB, 0 pin set for IOC (rising edge).
    banksel	IOCBP		;Battery Readings will be triggered from signal
    movwf	IOCBP		;sent from main ROV CPU.
    
main_loop
    
    
    ;Go into stndby mode:
    
    ;sleep
    
    goto	main_loop
   
    END                       























